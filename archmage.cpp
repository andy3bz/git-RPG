#include <iostream>
#include <stdlib.h>
#include <conio.h>
using namespace std;
int a,skill1=0,skill2=0,skill3=0,skill4=0,skills;
int cost1=0,cost2=0,cost3=0,cost4=0;
int level=1,upper=0,exp=200,drop=2000,gold,experience,limit=1000,togo;
int choose,hunt,sold,item,buy,buy1;
char option,lvling,name[20],job[20],gender,race[20];
bool loop=true,looper=true;

void load() {
    for(int i=0; i<=100; i++) {
        cout<<"\nLoading Exp..."<<i<<"%";
        system("cls");
    }
}

void regist() {
    do {
        system("cls");
        cout<<"Nickname     : "; cin.getline(name, 20);
        cout<<"Job          : "; cin.getline(job, 20);
        cout<<"Race         : "; cin.getline(race, 20);
        cout<<"Gender <m/f> : "; cin>>gender;
        cout<<"Do you want to begin the adventure <y/n> : "; cin>>lvling;
        cin.ignore();
    } while(lvling=='n');
    for(int i=0; i<=100; i++) {
        for(int j=0; j<=1; j++) {
            cout<<"\nLoading Game File..."<<i<<"%";
            system("cls");
        }
    }
}

void condition() {
    if(experience<limit) {
        togo=limit-experience;
        cout<<"( You Must Collect "<<togo<<" Experience to Level up !! )\n";
    } else if(experience==limit) {
        cout<<"Level up to level "<<level+1<<" !!\n";
        limit=limit+1000;
        level++;
        experience=0;
    }else if(experience>limit) {
        experience=experience-limit;
        cout<<"Level up to level "<<level+1<<" !!\n";
        limit=limit+1000;
        level++;
    } else {
        cout<<endl;
    }
}

void leveling() {
    system("cls");
    if(level>0 && level<=10) {
        load();
        cout<<"Dark Forest\n";
        cout<<"Exp Get      : "<<exp<<endl;
        experience=experience+exp;
        cout<<"Gold         : "<<drop<<endl;
        gold=gold+drop;
        cout<<"Press any key to continue...";
        system("pause>0");
    } else if(level>10 && level<=20) {
        load();
        cout<<"Gloomy Swamp\n";
        cout<<"Exp Get      : "<<exp<<endl;
        experience=experience+exp;
        cout<<"Gold         : "<<drop<<endl;
        gold=gold+drop;
        cout<<"Press any key to continue...";
        system("pause>0");
    } else if(level>20 && level<=30) {
        load();
        upper++;
        cout<<"Devil Mount\n";
        cout<<"Exp Get      : "<<exp<<endl;
        experience=experience+exp;
        cout<<"Gold         : "<<drop<<endl;
        gold=gold+drop;
        cout<<"Press any key to continue...";
        system("pause>0");
    } else if(level>30 && level<=40) {
        load();
        cout<<"Lost Dungeon\n";
        cout<<"Exp Get      : "<<exp<<endl;
        experience=experience+exp;
        cout<<"Gold         : "<<drop<<endl;
        gold=gold+drop;
        cout<<"Press any key to continue...";
        system("pause>0");
    } else if(level>40 && level<=50) {
        load();
        upper++;
        cout<<"Dragon Cave\n";
        cout<<"Exp Get      : "<<exp<<endl;
        experience=experience+exp;
        cout<<"Gold         : "<<drop<<endl;
        gold=gold+drop;
        cout<<"Press any key to continue...";
        system("pause>0");
    } else if(level>50 && level<=60) {
        load();
        cout<<"Lava Area\n";
        cout<<"Exp Get      : "<<exp<<endl;
        experience=experience+exp;
        cout<<"Gold         : "<<drop<<endl;
        gold=gold+drop;
        cout<<"Press any key to continue...";
        system("pause>0");
    } else if(level>60 && level<=70) {
        load();
        upper++;
        cout<<"Floating Island\n";
        cout<<"Exp Get      : "<<exp<<endl;
        experience=experience+exp;
        cout<<"Gold         : "<<drop<<endl;
        gold=gold+drop;
        cout<<"Press any key to continue...";
        system("pause>0");
    } else if(level>70 && level<=80) {
        load();
        cout<<"Ghost Dimension\n";
        cout<<"Exp Get      : "<<exp<<endl;
        experience=experience+exp;
        cout<<"Gold         : "<<drop<<endl;
        gold=gold+drop;
        cout<<"Press any key to continue...";
        system("pause>0");
    } else if(level>80 && level<=90) {
        load();
        upper++;
        cout<<"Dimension Limiter\n";
        cout<<"Exp Get      : "<<exp<<endl;
        experience=experience+exp;
        cout<<"Gold         : "<<drop<<endl;
        gold=gold+drop;
        cout<<"Press any key to continue...";
        system("pause>0");
    } else if(level>90 && level<=100) {
        load();
        upper++;
        cout<<"God Palace\n";
        cout<<"Exp Get      : "<<exp<<endl;
        experience=experience+exp;
        cout<<"Gold         : "<<drop<<endl;
        gold=gold+drop;
        cout<<"Press any key to continue...";
        system("pause>0");
    } else {
        cout<<"Level Maximum Reach !! Congratulations !!";
    }
}

void training() {
    do {
        system("cls");
        cout<<"Skill Point Left : "<<a<<endl;
        cout<<"1. Blizzard Skill    -> "<<skill1<<endl;
        cout<<"2. Fireball Skill    -> "<<skill2<<endl;
        cout<<"3. Wind Storm Skill  -> "<<skill3<<endl;
        cout<<"4. Earthquake Skill  -> "<<skill4<<endl;
        cout<<"\nChoose Skill You Want To Train : "; cin>>skills;
        switch(skills) {
        case 1 :
            cout<<"Skill Level Blizzard     : "<<skill1<<"\t<+1>"<<endl;
            cout<<"Cost Point <Skill Point> : "<<cost1<<endl;
            if(a!=0 && a>=cost1) {
                cout<<"Skill Level Up !!";
                skill1++;
                a=a-cost1;
                cost1=cost1+20;
            } else if(cost1 > a) {
                cout<<"Skill Point Less !!";
            } else {
                cout<<"Skill Point 'null' !!";
            }
            system("pause > 0");
            break;
        case 2 :
            cout<<"Skill Level Fireball     : "<<skill2<<"\t<+1>"<<endl;
            cout<<"Cost Point <Skill Point> : "<<cost2<<endl;
            if(a!=0 && a>=cost2) {
                cout<<"Skill Level Up !!";
                skill2++;
                a=a-cost2;
                cost2=cost2+20;
            } else if(cost2 > a) {
                cout<<"Skill Point Less !!";
            } else {
                cout<<"Skill Point 'null' !!";
            }
            system("pause > 0");
            break;
        case 3 :
            cout<<"Skill Level Wind Storm   : "<<skill3<<"\t<+1>"<<endl;
            cout<<"Cost Point <Skill Point> : "<<cost3<<endl;
            if(a!=0 && a>=cost3) {
                cout<<"Skill Level Up !!";
                skill3++;
                a=a-cost3;
                cost3=cost3+20;
            } else if(cost3 > a) {
                cout<<"Skill Point Less !!";
            } else {
                cout<<"Skill Point 'null' !!";
            }
            system("pause > 0");
            break;
        case 4 :
            cout<<"Skill Level Earthquake   : "<<skill4<<"\t<+1>"<<endl;
            cout<<"Cost Point <Skill Point> : "<<cost4<<endl;
            if(a!=0 && a>=cost4) {
                cout<<"Skill Level Up !!";
                skill4++;
                a=a-cost4;
                cost4=cost4+20;
            } else if(cost4 > a) {
                cout<<"Skill Point Less !!";
            } else {
                cout<<"Skill Point 'null' !!";
            }
            system("pause > 0");
            break;
        default :
            cout<<"No Skill Selected !!";
            system("pause > 0");
            break;
        }
    } while (loop==true);
}

void shop(){
    system("cls");
    cout<<"1. EXP Booster\n";
    cout<<"2. Gold Booster\n";
    cout<<"Choose   : "; cin>>item;
    switch(item) {
    case 1 :
        cout<<"Your Gold : "<<gold<<endl;
        cout<<"1. Fast Booster      <2000 G>\n";
        cout<<"2. Faster Booster    <20000 G>\n";
        cout<<"3. Fastest Booster   <200000 G>\n";
        cout<<"Buy      : "; cin>>buy;
        if(buy==1) {
            if(gold>=2000) {
                exp=exp+100;
                gold=gold-2000;
                cout<<"Bought !!";
                system("pause>0");
            } else {
                cout<<"Gold not Enough !!";
                system("pause>0");
            }
        } else if (buy==2) {
            if(gold>=20000) {
                exp=exp+300;
                gold=gold-20000;
                cout<<"Bought !!";
                system("pause>0");
            } else {
                cout<<"Gold not Enough !!";
                system("pause>0");
            }
        } else if (buy==3) {
            if(gold>=200000) {
                exp=exp+500;
                gold=gold-200000;
                cout<<"Bought !!";
                system("pause>0");
            } else {
                cout<<"Gold not Enough !!";
                system("pause>0");
            }
        } else {
            cout<<"Item not Found !!";
        }
        break;
    case 2 :
        cout<<"Your Gold : "<<gold<<endl;
        cout<<"1. Fast Booster      <1000 G>\n";
        cout<<"2. Faster Booster    <10000 G>\n";
        cout<<"3. Fastest Booster   <100000 G>\n";
        cout<<"Buy      : "; cin>>buy1;
        if(buy1==1) {
            if(gold>=2000) {
                drop=drop+100;
                gold=gold-1000;
                cout<<"Bought !!";
                system("pause>0");
            } else {
                cout<<"Gold not Enough !!";
                system("pause>0");
            }
        } else if (buy1==2) {
            if(gold>=2000) {
                drop=drop+300;
                gold=gold-10000;
                cout<<"Bought !!";
                system("pause>0");
            } else {
                cout<<"Gold not Enough !!";
                system("pause>0");
            }
        } else if (buy1==3) {
            if(gold>=2000) {
                drop=drop+500;
                gold=gold-100000;
                cout<<"Bought !!";
                system("pause>0");
            } else {
                cout<<"Gold not Enough !!";
                system("pause>0");
            }
        } else {
            cout<<"Item not Found !!";
        }
        break;
    default :
        break;
    }
}

void data() {
    system("cls");
    cout<<"-===RPG Simulator v1.0===-\n";
    condition();
    cout<<"Nickname     : "<<name<<endl;
    cout<<"Race         : "<<race<<endl;
    if(gender=='m') {
        cout<<"Gender       : Male\n";
    } else if(gender=='f') {
        cout<<"Gender       : Female\n";
    } else {
        cout<<"Gender       : Problem Gender !!\n";
    }
    if(upper<6) {
        cout<<"Class        : "<<job<<"\t<+"<<upper<<">\n";
    } else {
        cout<<"Class        : "<<job<<"\t<GOD>\n";
    }
    cout<<"Level        : "<<level<<endl;
    cout<<"Gold         : "<<gold<<endl;
    cout<<"Experience   : "<<experience<<"/"<<limit<<endl;
    cout<<"==========================\n\n";
    cout<<"1. Hunt\n";
    cout<<"2. Shop\n";
    cout<<"3. Train <Not Available>\n";
    cout<<"4. Exit\n";
    cout<<"Choose   : "; cin>>choose;
    switch(choose) {
    case 1 :
        leveling();
        break;
    case 2:
        shop();
        break;
    case 3 :
        training();
        break;
    case 4 :
        looper=false;
        cout<<"Back To Register !!";
        system("pause>0");
        break;
    default :
        cout<<"Not Available !!";
        break;
    }
}

main() {
    regist();
    do {
        data();
    } while(looper==true);
}
